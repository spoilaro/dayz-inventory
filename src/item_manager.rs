use std::process::exit;

use rusqlite::{Connection, Result};
use uuid::{uuid, Uuid};

#[derive(Debug)]
pub struct ItemRequest {
    request_id: String,
    item_name: String,
    count: usize,
}

impl std::fmt::Display for ItemRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "| {} | {} |", self.item_name, self.count)
    }
}

pub struct ItemManager {
    conn: Connection,
}

impl ItemManager {
    pub fn new() -> Self {
        let conn = Connection::open("item_requests.db").unwrap();
        match conn.execute(
            "CREATE TABLE required_items (
                request_id TEXT PRIMARY KEY,
                item_name TEXT NOT NULL,
                count INTEGER NOT NULL
            )",
            (),
        ) {
            Ok(_) => (),
            Err(err) => match err {
                // Table already exists
                rusqlite::Error::SqlInputError { .. } => (),
                err => {
                    println!("Unexpected error while creating a table, ERROR: {err:?}")
                }
            },
        };

        Self { conn }
    }

    pub fn new_request(&self, item_name: String, count: usize) -> Result<()> {
        let item_request = ItemRequest {
            request_id: Uuid::new_v4().to_string(),
            item_name,
            count,
        };

        self.conn.execute(
            "INSERT INTO required_items
                (request_id, item_name, count)
            VALUES
                (?1, ?2, ?3)
            ",
            (
                &item_request.request_id.to_string(),
                &item_request.item_name,
                &item_request.count,
            ),
        )?;

        Ok(())
    }

    pub fn all_requests(&self) -> Result<Vec<ItemRequest>> {
        let mut stmt = self
            .conn
            .prepare("SELECT request_id, item_name, count FROM required_items")?;
        let request_iterator = stmt
            .query_map([], |row| {
                let request_id = row.get(0)?;
                let item_name = row.get(1)?;
                let count = row.get(2)?;

                let request = ItemRequest {
                    request_id,
                    item_name,
                    count,
                };

                Ok(request)
            })
            .unwrap();

        let requests = request_iterator.map(|r| r.unwrap()).into_iter().collect();

        Ok(requests)
    }
}
