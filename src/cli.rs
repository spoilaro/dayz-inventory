use clap::{Subcommand, Parser};


#[derive(Subcommand, Debug)]
pub enum Commands {
    Add {

        /// Name of the item being added
        #[arg(short, long)]
        item_name: String, 

        /// Number of the items being added
        #[arg(short, long)]
        count: usize
    },

    List {
    }
}


#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct DayzItemizerCLI {

    #[command(subcommand)]
    pub command: Commands
}
