use clap::Parser;
// use std::io::{stdin, stdout};

use cli::{Commands, DayzItemizerCLI};
use item_manager::ItemManager;

mod cli;
mod item_manager;

fn main() {
    let itemizer = ItemManager::new();
    let cli = DayzItemizerCLI::parse();

    match &cli.command {
        Commands::Add { item_name, count } => {
            match itemizer.new_request(item_name.clone(), count.clone()) {
                Ok(_) => println!("Adding a new item was successful: {item_name}"),
                Err(error) => println!("Adding a new item failed, error: {error:?}")
            }
        }

        Commands::List {  } => {
            match itemizer.all_requests() {
                Ok(requests) => {
                    for request in requests {
                        println!("{}", request);
                    }
                },
                Err(error) => println!("Getting all requests failed, error: {error:?}")
            }
        }
    }

}
